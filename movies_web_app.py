import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():

    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies (id INT UNSIGNED NOT NULL AUTO_INCREMENT, \
                                      year TEXT, title TEXT, director TEXT, actor TEXT, \
                                      release_date TEXT, rating DOUBLE, \
                                      PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("INSERT INTO message (greeting) values ('Hello, World!')")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT * FROM movies")
    entries = [dict(id=row[0], year=row[1], title=row[2],
                    director=row[3], actor=row[4], 
                    release_date=row[5], rating=row[6]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_movie():
    print('Received request to add a movie')

    add_movie = ("INSERT INTO movies "
               "(year, title, director, actor, release_date, rating) "
               "VALUES (%s, %s, %s, %s, %s, %s)")
    movie_data = (request.form['year'], request.form['title'], request.form['director'],
                  request.form['actor'], request.form['release_date'], request.form['rating'])
    print('data: ' + str(movie_data))

    for datum in movie_data:
        if not datum:
            print('Movie %s could not be inserted - One or more fields were left empty.' % (request.form['title'],))
            return hello(['Movie %s could not be inserted - One or more fields were left empty.' % (request.form['title'],)])

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute('SELECT * FROM movies WHERE title = %s', (request.form['title'],))
        if len(cur.fetchall()):
            print('Movie %s could not be inserted - Movie already exists.' % (request.form['title'],))
            return hello(['Movie %s could not be inserted - Movie already exists.' % (request.form['title'],)])
        else:
            cur.execute(add_movie, movie_data)
            cnx.commit()
            print('Movie %s successfully inserted.' % (request.form['title']))
            return hello(['Movie %s successfully inserted.' % (request.form['title'])])
    except Exception as exp:
        print(exp)
        return hello(['Movie %s could not be inserted - %s' % (request.form['title'], str(exp))])


@app.route('/update_movie', methods=['POST'])
def update_movie():
    print('Received request to update a movie')

    movie_data = (request.form['year'], request.form['title'], request.form['director'],
                  request.form['actor'], request.form['release_date'], request.form['rating'])
    print('data: ' + str(movie_data))

    for datum in movie_data:
        if not datum:
            print('Movie %s could not be updated - One or more fields were left empty.' % (request.form['title'],))
            return hello(['Movie %s could not be updated - One or more fields were left empty.' % (request.form['title'],)])

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute('SELECT * FROM movies WHERE title = %s', (request.form['title'],))
        if not len(cur.fetchall()):
            print('Movie %s could not be updated - Movie does not exist.' % (request.form['title'],))
            return hello(['Movie %s could not be updated - Movie does not exist.' % (request.form['title'],)])
        else:
            cur.execute("UPDATE movies SET "
                   "year = %s, title = %s, director = %s, actor = %s," 
                   "release_date = %s, rating = %s WHERE title = %s", 
                   movie_data + (request.form['title'],))
            cnx.commit()
            print('Movie %s successfully updated.' % (request.form['title']))
            return hello(['Movie %s successfully updated.' % (request.form['title'])])
    except Exception as exp:
        print(exp)
        return hello(['Movie %s could not be updated - %s' % (request.form['title'], str(exp))])


@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print('Received request to delete a movie')
    delete_title = request.form['delete_title']

    if not delete_title:
        print('Movie %s could not be deleted - \'Title\' field was left empty' % (delete_title,))
        return hello(['Movie %s could not be deleted - \'Title\' field was left empty' % (delete_title,)])
    
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute('SELECT * FROM movies WHERE title = %s', (delete_title,))
        if not len(cur.fetchall()):
            return hello(['Movie %s does not exist' % (delete_title,)])
        else:
            cur.execute('DELETE FROM movies WHERE title = %s', (delete_title,))
            cnx.commit()
            return hello(['Movie %s successfully deleted' % (delete_title,)])
    except Exception as exp:
        print('Movie %s could not be deleted - %s' % (delete_title, str(exp)))
        return hello(['Movie %s could not be deleted - %s' % (delete_title, str(exp))])


@app.route('/search_movie', methods=['GET'])
def search_movie():
    print('Received request to search for a movie')
    search_actor = request.args['search_actor']
    
    if not search_actor:
        print('Could not search for a movie - \'Actor\' field was left empty')
        return hello(['Could not search for a movie - \'Actor\' field was left empty'])
    
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute('SELECT title, year, actor FROM movies WHERE actor = %s', (search_actor,))
        movies_found = cur.fetchall()
        if not len(movies_found):
            print ('No movies found for actor %s' % (search_actor,))
            return hello(['No movies found for actor %s' % (search_actor,)])
        else:
            for movie in movies_found:
                print ('%s, %s, %s' % (movie[0], movie[1], movie[2]))
            movie_list = ['%s, %s, %s' % (movie[0], movie[1], movie[2]) for movie in movies_found]
            return hello(['Movie(s) with %s:' % (search_actor,)] + movie_list)
    except Exception as exp:
        print('Could not search for a movie - %s' % (str(exp),))
        return hello(['Could not search for a movie - %s' % (str(exp),)])
    

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    print('Received request to get the highest rated movie')
    
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute('SELECT title, year, actor, director, rating FROM movies where rating = (SELECT MAX(rating) from movies)')
        max_movie = cur.fetchall()
        if not len(max_movie):
            print ('No movies are in the database. Try adding one.')
            return hello(['No movies are in the database. Try adding one.'])
        else:
            for movie in max_movie:
                print (movie)
            movie_list = [', '.join([str(field) for field in movie]) for movie in max_movie]
            return hello(['Movie(s) with the highest rating:'] + movie_list)
    except Exception as exp:
        print('Could not find the movie with the highest rating - %s' % (str(exp),))
        return hello(['Could not find the movie with the highest rating - %s' % (str(exp),)])


@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    print('Received request to get the lowest rated movie')
    
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute('SELECT title, year, actor, director, rating FROM movies where rating = (SELECT MIN(rating) from movies)')
        min_movie = cur.fetchall()
        if not len(min_movie):
            print ('No movies are in the database. Try adding one.')
            return hello(['No movies are in the database. Try adding one.'])
        else:
            for movie in min_movie:
                print (movie)
            movie_list = [', '.join([str(field) for field in movie]) for movie in min_movie]
            return hello(['Movie(s) with the lowest rating:'] + movie_list)
    except Exception as exp:
        print('Could not find the movie with the lowest rating - %s' % (str(exp),))
        return hello(['Could not find the movie with the lowest rating - %s' % (str(exp),)])


@app.route("/")
def hello(msg=[]):
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=entries, messages=msg)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
